package xyz.deployme.GUtils;

import static org.lwjgl.opengl.GL15.*;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;

public class Model
{
    private int vboVertexHandler;
    private int vertexNumber;
    private FloatBuffer vertexData;
    private int vboType;

    private int vboColorHandler;
    private int colorNumber;
    private FloatBuffer colorData;
    private boolean colorEnabled;

    private boolean shouldBeClosed;

    private void initData(Vertex[] verticies, int vboType, Color[] vertexColors, boolean colorEnabled)
    {
        /** Vertex data **/
        this.vboType = vboType;
        vboVertexHandler = glGenBuffers();
        vertexNumber = verticies.length;
        vertexData = BufferUtils.createFloatBuffer(verticies.length * Vertex.SIZE);
        for (Vertex v : verticies)
        {
            vertexData.put(v.getX());
            vertexData.put(v.getY());
            vertexData.put(v.getZ());
        }
        vertexData.flip();
        glBindBuffer(GL_ARRAY_BUFFER, vboVertexHandler);
        glBufferData(GL_ARRAY_BUFFER, vertexData, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        /** Color data **/
        this.colorEnabled = colorEnabled;
        if (colorEnabled) {
            vboColorHandler = glGenBuffers();
            colorNumber = vertexColors.length;
            colorData = BufferUtils.createFloatBuffer(vertexColors.length * Color.SIZE);
            for (Color c : vertexColors) {
                colorData.put(c.getR());
                colorData.put(c.getG());
                colorData.put(c.getB());

            }
            colorData.flip();
            glBindBuffer(GL_ARRAY_BUFFER, vboColorHandler);
            glBufferData(GL_ARRAY_BUFFER, colorData, GL_STATIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
        shouldBeClosed = true;
    }

    public Model(Vertex[] verticies, int vboType, Color[] vertexColors, boolean colorEnabled)
    {
        initData(verticies, vboType, vertexColors, colorEnabled);
    }

    public Model(float[] rawVerticies, int vboType, Color[] vertexColors, boolean colorEnabled)
    {
        int length = rawVerticies.length;
        if(length % 3 != 0)
            length = length / 3 * 3;
        Vertex[] temp = new Vertex[length / 3];
        for (int i = 0; i < length; i += 3)
            temp[i / 3] = new Vertex(rawVerticies[i], rawVerticies[i + 1], rawVerticies[i + 2]);
        initData(temp, vboType, vertexColors, colorEnabled);
    }

    public int getVertexNumber()
    {
        return vertexNumber;
    }
    public int getColorNumber()
    {
        return colorNumber;
    }
    public int getVboColorHandler()
    {
        return vboColorHandler;
    }
    public int getVertexHandler()
    {
        return vboVertexHandler;
    }
    public int getVboType()
    {
        return vboType;
    }
    public boolean getColorEnabled()
    {
        return colorEnabled;
    }

    public void close()
    {
        if (shouldBeClosed) {
            glDeleteBuffers(this.vboColorHandler);
            glDeleteBuffers(this.vboVertexHandler);
            shouldBeClosed = false;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            if (shouldBeClosed) {
                System.out.println("GPU-MEMORY LEAK: GPU buffers were not deleted before garbage collection!");
                this.close();
            }
        }
        finally {
            super.finalize();
        }

    }
}