package xyz.deployme.DataGen;

import xyz.deployme.GUtils.Color;
import xyz.deployme.GUtils.Model;
import xyz.deployme.GUtils.Vertex;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by rpreda on 15/02/17.
 */
public class ModelGenerator {

    public static Model GenerateModel(Vertex center, float radius)
    {

        Vertex xp = new Vertex(center.getX() + radius, center.getY(), center.getZ());
        Vertex xn = new Vertex(center.getX() - radius, center.getY(), center.getZ());

        Vertex yp = new Vertex(center.getX(), center.getY() + radius, center.getZ());
        Vertex yn = new Vertex(center.getX(), center.getY() - radius, center.getZ());

        Vertex zp = new Vertex(center.getX(), center.getY(), center.getZ() + radius);
        Vertex zn = new Vertex(center.getX(), center.getY(), center.getZ() - radius);

        List<Vertex> vertices = new ArrayList<>();

        vertices.add(xp);
        vertices.add(zp);
        vertices.add(yp);

        vertices.add(zp);
        vertices.add(xn);
        vertices.add(yp);

        vertices.add(xn);
        vertices.add(zn);
        vertices.add(yp);

        vertices.add(zn);
        vertices.add(xp);
        vertices.add(yp);

        vertices.add(zn);
        vertices.add(xn);
        vertices.add(yp);

        /** Negative Y **/

        vertices.add(xp);
        vertices.add(zp);
        vertices.add(yn);

        vertices.add(zp);
        vertices.add(xn);
        vertices.add(yn);

        vertices.add(xn);
        vertices.add(zn);
        vertices.add(yn);

        vertices.add(zn);
        vertices.add(xp);
        vertices.add(yn);

        vertices.add(zn);
        vertices.add(xn);
        vertices.add(yn);

        return new Model(vertices.toArray(new Vertex[vertices.size()]), GL_TRIANGLES, Color.genRandColorsStrong(/**0, 255, 0,**/ 10 * 3), true);
    }

    public static Model[] GenerateMultipleModels(Vertex startVertex, float radius, float spacing, int x_number, int y_number, int z_number)
    {
        List<Model> ret_val = new ArrayList<>();
        for (int i = 0; i < x_number; i++)
            for (int j = 0; j < z_number; j++)
                for (int k = 0; k < y_number; k++) {
                    ret_val.add(ModelGenerator.GenerateModel(
                       new Vertex(
                               startVertex.getX() + spacing * i,
                               startVertex.getZ() + spacing * j,
                               startVertex.getY() + spacing * k
                       ),
                       radius

                    ));
                }
        return ret_val.toArray(new Model[ret_val.size()]);
    }
}
