package xyz.deployme;

import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;
import xyz.deployme.CallbackHandlers.ResizeHandler;
import xyz.deployme.GUtils.Color;
import xyz.deployme.GUtils.Model;
import xyz.deployme.GUtils.Vertex;
import xyz.deployme.CallbackHandlers.KeyboardHandler;
import xyz.deployme.CallbackHandlers.ScrollHandler;
import xyz.deployme.DataGen.ModelGenerator;

import java.nio.*;
import java.util.Random;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Main {
    private long window;
    private GLFWKeyCallback keyCallback;
    private GLFWScrollCallback scrollCallback;
    public static Random rand;
    /** WARNING: World will look skewed if height > width **/
    public static int width = 1280, height = 720;
    public static float heightWidthRatio = (float)height/width;
    public static boolean shouldResetModelView = true;

    public void run() {
        System.out.println("LWJGL " + Version.getVersion());
        init();
        loop();
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

    private void init() {
        GLFWErrorCallback.createPrint(System.err).set();
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
        window = glfwCreateWindow(width, height, "RobotAI", NULL, NULL);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");
        glfwSetKeyCallback(window, keyCallback = new KeyboardHandler());
        glfwSetScrollCallback(window, scrollCallback = new ScrollHandler());
        glfwSetWindowSizeCallback(window, new ResizeHandler());
        try (MemoryStack stack = stackPush()) {
            IntBuffer pWidth = stack.mallocInt(1);
            IntBuffer pHeight = stack.mallocInt(1);
            glfwGetWindowSize(window, pWidth, pHeight);
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            );
        }
        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);
        glfwShowWindow(window);
    }

    public void update(){
        if(KeyboardHandler.isKeyDown(GLFW_KEY_SPACE))
            System.out.println("Space Key Pressed");
        if (KeyboardHandler.isKeyDown(GLFW_KEY_ESCAPE))
            glfwSetWindowShouldClose(window, true);

        /** Key Controls **/
        if (KeyboardHandler.isKeyDown(GLFW_KEY_E))
            glScalef(0.95f, 0.95f, 0.95f);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_Q))
            glScalef(1.05f, 1.05f, 1.05f);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_W))
            glRotatef(1, 1, 0, 0);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_S))
            glRotatef(1, -1, 0, 0);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_A))
            glRotatef(1, 0, 1, 0);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_D))
            glRotatef(1, 0, -1, 0);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_Z))
            glRotatef(1, 0, 0, 1);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_X))
            glRotatef(1, 0, 0, -1);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_2))
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        if (KeyboardHandler.isKeyDown(GLFW_KEY_1))
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        if (KeyboardHandler.isKeyDown((GLFW_KEY_R)) || shouldResetModelView) {
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-1, 1, -1 * heightWidthRatio, 1 * heightWidthRatio, -100 , 100);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            glViewport(0, 0, width * 2, height * 2);
            shouldResetModelView = false;
        }
        //TODO: remove debug key
        if (KeyboardHandler.isKeyDown(GLFW_KEY_K))
            System.out.println("DEBUG KEY");
    }

    private void render(Model[] vertexObjects)
    {
        glColor3f(0, 1, 0);
        for (Model vbo : vertexObjects) {//Render every VBO in the array

            glBindBuffer(GL_ARRAY_BUFFER, vbo.getVertexHandler());
            glVertexPointer(Vertex.SIZE, GL_FLOAT, 0, 0L);

            if (vbo.getColorEnabled()) {
                glBindBuffer(GL_ARRAY_BUFFER, vbo.getVboColorHandler());
                glColorPointer(Color.SIZE, GL_FLOAT, 0, 0L);
            }

            glEnableClientState(GL_VERTEX_ARRAY);
            if (vbo.getColorEnabled())
                glEnableClientState(GL_COLOR_ARRAY);
            glDrawArrays(vbo.getVboType(), 0, vbo.getVertexNumber());//Tied vbo type to objects
            if (vbo.getColorEnabled())
                glEnableClientState(GL_COLOR_ARRAY);
            glDisableClientState(GL_VERTEX_ARRAY);

            glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
    }

    private void loop() {
        GL.createCapabilities();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glEnable(GL_DEPTH_TEST);

        Model[] data = ModelGenerator.GenerateMultipleModels(new Vertex(-10.4f, -10.4f, 0), 0.1f, 0.8f, 25, 25, 25);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        long millis;
        long fpsUpdateTimer = System.currentTimeMillis();
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(10, 10, 10, 10, -10, -10);

        while (!glfwWindowShouldClose(window)) {

            millis = System.currentTimeMillis();
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            render(data);
            glfwSwapBuffers(window);
            glfwPollEvents();
            update();
            if (millis - fpsUpdateTimer > 100) {
                fpsUpdateTimer = millis;
                glfwSetWindowTitle(window, "FPS: " + (int)(1000f / (float) (System.currentTimeMillis() - millis)));
            }

        }
    }
    public static void main(String[] args) {
        rand = new Random();
        new Main().run();
    }
}
