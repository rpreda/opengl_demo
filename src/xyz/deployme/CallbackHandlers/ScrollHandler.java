package xyz.deployme.CallbackHandlers;

import org.lwjgl.glfw.GLFWScrollCallback;
public class ScrollHandler extends GLFWScrollCallback{

    private static byte verticalDirection;
    @Override
    public void invoke(long window, double xoffset, double yoffset)
    {
        if (yoffset > 3)
            verticalDirection = 1;
        else if (yoffset < 3)
            verticalDirection = -1;
        else
            verticalDirection = 0;
    }
    public static byte verticalScrollDirection()
    {
        return verticalDirection;
    }
}
