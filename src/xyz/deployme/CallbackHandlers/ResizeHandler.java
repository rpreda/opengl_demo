package xyz.deployme.CallbackHandlers;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import xyz.deployme.Main;

/**
 * Created by rpreda on 17/02/17.
 */
public class ResizeHandler extends GLFWWindowSizeCallback{

    @Override
    public void invoke(long windows, int x, int y)
    {
        Main.width = x;
        Main.height = y;
        Main.heightWidthRatio = (float)y/x;
        Main.shouldResetModelView = true;
    }
}
