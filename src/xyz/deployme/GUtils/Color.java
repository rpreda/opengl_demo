package xyz.deployme.GUtils;

import xyz.deployme.Main;

/**
 * Created by rpreda on 15/02/17.
 */
public class Color {
    public static final int SIZE = 3;
    private float r, g, b;

    public static Color[] genRandColors(int number)
    {
        Color[] col = new Color[number];
        for (int i = 0; i < number; i++)
            col[i] = new Color(Main.rand.nextInt(205) + 50, Main.rand.nextInt(205) + 50, Main.rand.nextInt(205) + 50);
        return col;
    }

    public static Color[] genRandColorsStrong(int number)
    {
        Color[] colorTab = new Color[]{
                new Color(255, 0 , 0), new Color(0, 255, 0), new Color(0, 0, 255),
                new Color(255, 255 , 0), new Color(0, 255, 255), new Color(255, 0, 255)};
        Color[] col = new Color[number];
        for (int i = 0; i < number; i++)
            col[i] = colorTab[Main.rand.nextInt(colorTab.length)];
        return col;
    }

    public static Color[] genStaticColor(int r, int g, int b, int number)
    {
        Color[] ret_val = new Color[number];
        for (int i = 0; i < number; i++)
            ret_val[i] = new Color(r, g, b);
        return ret_val;
    }

    private int clamp(int value)
    {
        if (value >= 0 && value <= 255)
            return value;
        if (value < 0)
            return 0;
        if (value > 255)
            return 255;
        return 0;
    }

    public Color(int r, int g, int b)
    {
        r = clamp(r);
        g = clamp(g);
        b = clamp(b);
        this.r = (float)r /255;
        this.g = (float)g /255;
        this.b = (float)b /255;
    }

    public float getR() {
        return r;
    }
    public float getG() {
        return g;
    }
    public float getB() {
        return b;
    }
}
